﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGun
{
    void Flip(bool fliped);

    void StartShooting();
    void StopShooting();

    void Remove();
}
