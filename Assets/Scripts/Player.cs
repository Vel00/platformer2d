﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : Entity
{
    [Header("Jump")]
    public float jumpForce = 15f;//сила прыжка
    public int jumps = 2;//прыжки
    public int jumpsCount;

    [Header("Ground check")]
    public bool wasGrounded = true;
    public bool isGrounded = false;
    public LayerMask whatIsGround;
    public Transform groundCheck;
    public float checkRadius;

    [Header("Gravity")]
    public float normalGravity = 1f;
    public float fallGravity = 3f;

    [Header("Guns")]
    public IGun gun;

    [Header("Sounds")]
    public AudioSource jumpSound;
    public AudioSource fallSound;
    public AudioSource stepSound;

    [Header("Other")]
    public CameraController camera;

    private SpriteRenderer sprite;
    private Animator anim;

    public static Player Instance { get; set; }

    public enum States
    {
        idle,
        run,
        jump,
        fall
    }

    private States State
    {
        get { return (States)anim.GetInteger("state"); }
        set { anim.SetInteger("state", (int)value); }
    }

    private void Start()
    {
        jumpsCount = jumps;
    }
    

    protected override void Awake()
    {
        base.Awake();

        sprite = GetComponentInChildren<SpriteRenderer>();
        anim = GetComponent<Animator>();

        Instance = this;
    }

    private void FixedUpdate()
    {
        CheckGround();
    }


    private void Update()
    {
        if (alive)
        {
            if (isGrounded) State = States.idle;

            if (Input.GetButton("Horizontal"))
                Run();
            if (Input.GetButtonDown("Jump"))
                Jump();
            if (isGrounded == true)
            {
                jumpsCount = jumps;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
            }

            if (State == States.run)
            {
                if (!stepSound.isPlaying)
                    stepSound.Play();
            }
            else if (stepSound.isPlaying)
                stepSound.Pause();

            if (!wasGrounded && isGrounded)
                fallSound.Play();

            wasGrounded = isGrounded;
        }
    }

    private void Run()
    {
        if(isGrounded) State = States.run;

        Vector3 dir = transform.right * Input.GetAxis("Horizontal");

        transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, speed * Time.deltaTime);

        sprite.flipX = dir.x < 0.0f;
    }


    private void CheckGround()
    {
        Collider2D[] collider = Physics2D.OverlapCircleAll(groundCheck.position, 0.3f, whatIsGround);
        isGrounded = collider.Length > 0;

        if (!isGrounded)
        {
            if (rb.velocity.y < 0)
            {
                State = States.fall;
                rb.gravityScale = fallGravity;
            }
            else
                State = States.jump;
        }
        else
            rb.gravityScale = normalGravity;
    }


    private void Jump()
    {
        jumpsCount--;

        if (jumpsCount > 0)
        {
            rb.gravityScale = normalGravity;
            rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
        }

        if (isGrounded)
        {
            jumpSound.Play();
        }
    }

    public override bool GetDamage(int damage)
    {
        alive = false;

        StartCoroutine(Restart());
        return false;
    }

    private IEnumerator Restart()
    {
        yield return new WaitForSeconds(0.35f);

        alive = true;
        LevelManager.Instance.Restart();
    }

    public void OnDrawGizmosSelected()
    {
        UnityEditor.Handles.DrawWireDisc(groundCheck.position, Vector3.back, checkRadius);
    }

    public void ForcePosition(Vector3 position)
    {
        transform.position = position;

        camera.Jump();

        rb.velocity = Vector3.zero;
    }
}

