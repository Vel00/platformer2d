﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float speed = 2;
    public Transform player;
        
    private void Awake()
    {
        if(!player)
        {
            player = Player.Instance.transform;
        }
    }

    private void Update()
    {
        Track();
    }

    private void Track()
    {
        Vector3 pos = player.position;
        pos.z = -10f;

        transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * speed);
    }

    public void Jump()
    {
        transform.position = player.position;
    }
}
