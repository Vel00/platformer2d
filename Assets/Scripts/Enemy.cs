﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

public class Enemy : Entity
{
    [Header("Enemy AI")]
    public Transform detectionCenter;
    public float detectionRadius;
    public LayerMask detectionLayer;

    [Header("Attack")]
    public Transform attackCenter;
    public float attackRadius;
    public Collider2D attackCollider;

    public float attackDelay = 1f;
    public int damage = 1;

    [Header("Sound")]
    public AudioSource damageSound;
    public AudioSource deathSound;
    public AudioSource slashStartSound;
    public AudioSource slashSound;

    private float attackTimer = 0;

    private Vector3 dir;

    private Collider2D collider;
    private SpriteRenderer sprite;
    private Animator anim;
    private LayerMask Dead;

    public enum States
    {
        idle,
        walk
    }

    private States State
    {
        get { return (States)anim.GetInteger("state"); }
        set { anim.SetInteger("state", (int)value); }
    }

    protected override void Awake()
    {
        base.Awake();
        sprite = GetComponentInChildren<SpriteRenderer>();

        anim = GetComponent<Animator>();
        collider = GetComponent<Collider2D>();

        Dead = LayerMask.NameToLayer("Dead");
    }

    void Start()
    {
        dir = transform.right;
    }

    void Update()
    {

        if (alive)
        {
            Move();
            TryAttack();
        }
    }

    private void Move()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(detectionCenter.position, detectionRadius, detectionLayer);

        if (attackTimer <= 0 && colliders.Length > 0)
        {
            Vector3 target = colliders.First().gameObject.transform.position;

            dir = target - transform.position;

            dir.y = 0;
            dir.z = 0;

            dir.Normalize();

            transform.localEulerAngles = new Vector3(0, dir.x < 0 ? 180 : 0, 0);

            transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, Time.deltaTime * speed);

            State = States.walk;
        }
        else
        {
            State = States.idle;
        }
    }

    private void TryAttack()
    {
        attackTimer -= Time.deltaTime;

        if (attackTimer <= 0)
        {

            Collider2D[] colliders = Physics2D.OverlapCircleAll(attackCenter.position, attackRadius, detectionLayer);

            if (colliders.Length > 0)
            {
                attackTimer = attackDelay;
                StartCoroutine(Attack());
                
            }
        }
    }

    protected IEnumerator Attack()
    {
        List<Collider2D> colliders = new List<Collider2D>();

        attackCollider.OverlapCollider(
            new ContactFilter2D()
            {
                useLayerMask = true,
                layerMask = detectionLayer
            },

            colliders);

        if (colliders.Count > 0)
        {
            anim.SetTrigger("attack");
            slashStartSound.Play();

            yield return new WaitForSeconds(0.4f);

            Collider2D collider = colliders.First();
            collider.gameObject.GetComponent<Entity>().GetDamage(damage);

            slashSound.Play();
        }
    }

    private void DealDamage()
    {

    }

    public override bool GetDamage(int damage)
    {
        if (alive)
        {
            bool dead = base.GetDamage(damage);

            if (!dead)
            {
                damageSound.Play();
                anim.SetTrigger("takehit");
            }

            return dead;
        }

        return true;
    }

    public override void Die()
    {
        anim.SetTrigger("death");
        alive = false;
        gameObject.layer = Dead;
        gameObject.tag = "NoBullet";
        deathSound.Play();
    }

    /*
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == Player.Instance.gameObject)
        {
            Player.Instance.GetDamage(damage);
        }
    }
    */

    public void OnDrawGizmosSelected()
    {
        UnityEditor.Handles.DrawWireDisc(detectionCenter.position, Vector3.back, detectionRadius);
        UnityEditor.Handles.DrawWireDisc(attackCenter.position, Vector3.back, attackRadius);
    }
}
