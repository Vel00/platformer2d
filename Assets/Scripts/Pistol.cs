﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Gun, IGun
{
    public void StartShooting()
    {
        SpawnBullet();
        shotSound.Play();
    }

    public void StopShooting()
    {

    }
}
