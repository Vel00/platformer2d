﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Belt : MonoBehaviour
{
    public Transform hands;

    public List<GameObject> guns = new List<GameObject>();

    public int currentGun = 0;

    void Start()
    {
        SetCurrentGun(0);
    }

    void Update()
    {
        if (Input.GetButtonDown("Gun1"))
        {
            SetCurrentGun(0);
        }
        else if (Input.GetButtonDown("Gun2"))
        {
            SetCurrentGun(1);
        }
    }

    public void SetCurrentGun(int index)
    {
        index = index % guns.Count;
        currentGun = index;

        Player.Instance.gun?.Remove();
        GameObject newGun = Instantiate(guns[index], hands);
        Player.Instance.gun = newGun.GetComponent<IGun>();
    }

    public void Next()
    {
        SetCurrentGun(currentGun + 1);
    }

    public void Previous()
    {
        SetCurrentGun(currentGun - 1);
    }
}
