﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssaultRifle : Gun, IGun
{
    public float shootDelay = 0.1f;
    private bool isShooting = false;
    private float shootTimer = 0;

    private void Start()
    {
        shootTimer = shootDelay;
    }

    private void Update()
    {
        if (isShooting)
        {
            shootTimer += Time.deltaTime;

            if (shootTimer >= shootDelay)
            {
                shootTimer -= shootDelay;
                SpawnBullet();
                shotSound.Play();
            }
        }
    }

    public void StartShooting()
    {
        isShooting = true;
    }

    public void StopShooting()
    {
        isShooting = false;
        shootTimer = shootDelay;
    }
}
