﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [Header("Unity")]
    public SpriteRenderer sprite;
    public Transform shootPoint;
    public GameObject bullet;

    [Header("Sound")]
    public AudioSource shotSound;

    [Header("Gun")]
    public float bulletSpeed = 20f;
    public int bulletDamage = 3;

    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    public void Flip(bool fliped)
    {
        transform.localEulerAngles = new Vector3(fliped?180:0, 0, 0);
    }

    protected void SpawnBullet()
    {
        GameObject bulletObject = Instantiate(bullet, shootPoint.position, shootPoint.rotation);
        Bullet b = bulletObject.GetComponent<Bullet>();

        b.speed = bulletSpeed;
        b.damage = bulletDamage;
    }

    public void Remove()
    {
        Destroy(gameObject);
    }
}
