﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Danger : MonoBehaviour
{
    public int damage = 1000;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        collider.GetComponent<Entity>()?.GetDamage(damage);
    }
}
