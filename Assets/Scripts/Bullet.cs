﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 20f;
    public int damage = 3;
    public Rigidbody2D rb;
    public SpriteRenderer sr;

    [Header("Sounds")]
    public AudioSource metallHit;

    private bool impacted = false;

    void Start()
    {
        rb.velocity = transform.right * speed;
    }
    
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!impacted)
        {
            if (collision.CompareTag("NoBullet"))
                return;

            impacted = true;

            Entity entity = collision.GetComponent<Entity>();

            if (entity != null)
            {
                if (entity is Player)
                {
                    return;
                }

                entity.rb.AddForce(transform.right * speed * 0.02f, ForceMode2D.Impulse);

                entity.GetDamage(damage);
            }
            else
                metallHit.Play();

            rb.velocity = Vector2.zero;
            sr.enabled = false;

            Destroy(gameObject, 1);
        }
        
    }
}
