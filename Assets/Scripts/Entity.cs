﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    [Header("Unity")]
    public Rigidbody2D rb;

    [Header("Stats")]
    public bool alive = true;
    public int lives = 5;
    public float speed = 3.5f;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public virtual bool GetDamage(int damage)
    {
        lives -= damage;
        if (lives <= 0)
        {
            Die();
            return true;
        }

        return false;
    }

    public virtual void Die()
    {
        alive = false;
        Destroy(this.gameObject);
    }
}
