﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerAim : MonoBehaviour
{
    private Transform aimTransform;
    public Camera mainCamera;
    

    public float angle;

    private void Awake()
    {
        aimTransform = transform.Find("Aim");
    }
    
    void Start()
    {
        
    }

    void Update()
    {
        Aim();
        HandleShoot();
    }

    void Aim()
    {
        Vector3 mousePosition = GetMousePosition();

        Vector3 aimDirection = (mousePosition - aimTransform.position).normalized;

        angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;

        aimTransform.eulerAngles = new Vector3(0, 0, angle);

        Player.Instance.gun?.Flip(Mathf.Abs(angle) > 90);
        
    }

    void HandleShoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Player.Instance.gun?.StartShooting();
        }
        if (Input.GetMouseButtonUp(0))
        {
            Player.Instance.gun?.StopShooting();
        }
    }

    private Vector3 GetMousePosition()
    {
        Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0;
        return mousePosition;
    }
}
