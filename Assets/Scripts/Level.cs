﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Transform entry;
    public Door exit;
    public List<Transform> enemyPositions;

    private List<GameObject> enemies = new List<GameObject>();

    public void StartLevel()
    {
        Player.Instance.ForcePosition(entry.position);

        foreach (var position in enemyPositions)
        {
            GameObject enemy = Instantiate(LevelManager.Instance.enemyPrefab, position.position, Quaternion.identity, transform);
            enemies.Add(enemy);
        }
    }

    public void CleanLevel()
    {
        foreach(var enemy in enemies)
        {
            Destroy(enemy);
        }

        enemies.Clear();
    }


}
