﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public List<Level> levels = new List<Level>();
    public int currentIndex = 0;

    private Level currentLevel
    {
        get => levels[currentIndex];
    }

    public static LevelManager Instance;

    public GameObject enemyPrefab;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        currentLevel.StartLevel();
    }

    public void Restart()
    {
        currentLevel.CleanLevel();
        currentLevel.StartLevel();
    }

    public void NextLevel()
    {
        currentLevel.CleanLevel();
        currentIndex += 1;

        if (currentIndex >= levels.Count)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        else
            currentLevel.StartLevel();
    }
}
